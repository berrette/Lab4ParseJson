//
//  ViewController.swift
//  STARwarsLaborationRealOne
//
//  Created by Christofer Berrette on 2017-11-19.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var ready = 0
    var fetchedstawazz = [Stawaxx]()
    @IBOutlet weak var textlbl: UILabel!

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidAppear(_ animated: Bool) {
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        
        fetchedstawazz = []
        if let url = URL(string: "https://swapi.co/api/people/"){
            
            //handle the request
            
            let task = URLSession.shared.dataTask(with: url) { (data, respone, error) in
                if error != nil
                {
                    print(error as Any)
                }
                else
                {
                    if let urlContent = data
                    {
                        do{
                            //process the json we fetch
                            let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as? AnyObject
                            // print(jsonResult)
                            if let dict = jsonResult as? Dictionary<String,AnyObject>{
                                print(dict)
                                
                                if let results = dict["results"] as? [Dictionary<String, AnyObject>]{
                                    
                                    for result in results{
                                        
                                        if let name = result["name"]{
                                            if let height = result["height"]{
                                                if let mass = result["mass"]{
                                                    if let gender = result["gender"]{
                                                        if let skinColor = result["skin_color"]{
                                                            self.fetchedstawazz.append(Stawaxx(name: name as! String, height: height as! String, mass: mass as! String, gender: gender as! String, skinColor: skinColor as! String))
                                                        }
                                                    }
                                                }
                                            }
                                           
                                        }
                                        
                                        
                                        
                                        
                                        
                                       
                                        
                                        
                                        
                                        
                                       
                                        
                                    }
                                     print(self.fetchedstawazz)
                                    
                                    OperationQueue.main.addOperation {
                                        self.tableView.reloadData()
                                    }
                                   
                                }
                            }
                            
                            
                            
                        }catch
                        {
                            print("JSSOn Failde")
                        }
                    }
                }
                
            }
            task.resume()
        } else
        {
            //showWeather.text = "errror errro";
        }
   
        
    
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedstawazz.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellStawarsname", for: indexPath)
        
        cell.textLabel?.text = fetchedstawazz[indexPath.row].name
        
        return cell
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "details"{
            if let indexPath = tableView.indexPathForSelectedRow
            {
                let destinationVC = segue.destination as! details
                
                destinationVC.name = fetchedstawazz[indexPath.row].name
                destinationVC.height = fetchedstawazz[indexPath.row].height
                destinationVC.mass = fetchedstawazz[indexPath.row].mass
                destinationVC.gender = fetchedstawazz[indexPath.row].gender
                destinationVC.skinColor = fetchedstawazz[indexPath.row].skinColor
               
            }
        }
    }

}

class Stawaxx {
    var name: String
    var height: String
    var mass: String
    var gender: String
    var skinColor: String
    
    init(name: String, height: String, mass: String, gender: String, skinColor: String ) {
        self.name = name
        self.height = height
        self.mass = mass
        self.gender = gender
        self.skinColor = skinColor
        
    }
}

