//
//  details.swift
//  STARwarsLaborationRealOne
//
//  Created by Christofer Berrette on 2017-11-19.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

class details: UIViewController {

    var stawax = [Stawaxx]()
    
    var name = ""
    var height = ""
    var mass = ""
    var gender = ""
    var skinColor = ""
    
    
    @IBOutlet weak var nameS: UILabel!
    @IBOutlet weak var heightS: UILabel!
    @IBOutlet weak var massS: UILabel!
    @IBOutlet weak var genderS: UILabel!
    
    @IBOutlet weak var skinColorS: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print(name)
        print(skinColor)
        
        nameS.text = name
        heightS.text = ("Height: \(height)")
        massS.text = ("Mass: \(mass)")
        genderS.text = ("Gender: \(gender)")
        skinColorS.text = ("Skin Color: \(skinColor)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
